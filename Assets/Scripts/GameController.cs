﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleInputNamespace;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public GameObject ball;
    public GameObject destroyer;
    public GameObject destination;
    Vector3 dir;
    public float moveX;
    public float moveY;
    public Vector3 startPos;
    public Vector3 lastPos;
    public GameObject winCanvas;
    public GameObject loseCanvas;
    public bool isEndGame = false;

    void Start()
    {
        Application.targetFrameRate = 60;
        instance = this;
        ball = GameObject.FindGameObjectWithTag("Ball");
        destination = GameObject.FindGameObjectWithTag("Finish");
        dir = Vector3.zero;
    }

    void FixedUpdate()
    {
        if (Joystick.joystickHeld)
        {
            startPos = Input.mousePosition;
            dir.x = SimpleInput.GetAxis("Horizontal");
            dir.y = SimpleInput.GetAxis("Vertical");

            if (dir != Vector3.zero && startPos != lastPos)
            {
                destroyer.transform.position = new Vector3(destroyer.transform.position.x + dir.x, destroyer.transform.position.y + dir.y, destroyer.transform.position.z);
            }
            lastPos = startPos;
        }
        destroyer.transform.position = new Vector3(Mathf.Clamp(destroyer.transform.position.x, -4.5f, 4.5f), Mathf.Clamp(destroyer.transform.position.y, destination.transform.position.y, 7f), 0.1f);
        if (ball != null)
        {
            transform.position = Vector3.Slerp(transform.position, new Vector3(0, ball.transform.position.y - 5, -36), 5 * Time.deltaTime);
        }
        transform.position = new Vector3(0, Mathf.Clamp(transform.position.y, destination.transform.position.y + 9, 0f), -36);
    }

    public void Win()
    {
        if (!isEndGame)
        {
            winCanvas.SetActive(true);
            isEndGame = true;
        }
    }

    public void Lose()
    {
        if (!isEndGame)
        {
            loseCanvas.SetActive(true);
            isEndGame = true;
        }
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }
}
