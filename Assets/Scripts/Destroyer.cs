﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public GameObject explodeEffect;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Point")
        {
            Destroy(other.gameObject);
            Instantiate(explodeEffect, other.transform.position, Quaternion.identity);    
        }
    }
}
