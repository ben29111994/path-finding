﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject explodeEffect;

    private void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -5.5f, 5.5f), transform.position.y, - 0.55f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Red")
        {
            GameController.instance.Lose();
            Instantiate(explodeEffect, transform.position, Quaternion.identity);
            Destroy(gameObject, 0.1f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            GameController.instance.Win();
        }
    }
}
